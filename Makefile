# File: /Makefile
# Project: @risserlabs/keycloak-avatar-client
# File Created: 31-07-2022 14:43:01
# Author: Clay Risser
# -----
# Last Modified: 16-08-2022 11:56:54
# Modified By: Clay Risser
# -----
# Risser Labs LLC (c) Copyright 2022

include mkpm.mk
ifneq (,$(MKPM_READY))
include $(MKPM)/gnu
include $(MKPM)/mkchain
include $(MKPM)/envcache
include $(MKPM)/dotenv

export PATCHES := \
	keycloak/js/package.json \
	keycloak/js/libs/keycloak-js/package.json \
	keycloak/js/libs/keycloak-js/src/keycloak.js

include $(MKPM)/patch

export CLOC ?= cloc
export NPM ?= npm
export YARN ?= yarn
export TMPDIR=/tmp

ACTIONS += submodules
SUBMODULES_TARGETS := keycloak/README.md
keycloak/README.md:
	@$(call reset,submodules)
$(ACTION)/submodules:
	@$(GIT) submodule update --init --recursive
	@$(call done,submodules)

ACTIONS += patch~submodules
$(ACTION)/patch: $(call git_deps,\.patch$$)
	@$(MAKE) -s patch-apply
	@$(call done,patch)

ACTIONS += install~patch
INSTALL_TARGETS := keycloak/js/package.json
keycloak/js/package.json:
	@$(call reset,install)
$(ACTION)/install: 
	@$(CD) keycloak/js && \
		$(YARN) $(ARGS)
	@$(call done,install)

ACTIONS += build~install
$(ACTION)/build: 
	@$(CD) keycloak/js/libs/keycloak-js && \
		$(YARN) build $(ARGS)
	@$(call done,build)

.PHONY: publish
publish: | ~build +publish
+publish: 
	@$(CD) keycloak/js/libs/keycloak-js && \
		$(NPM) publish $(ARGS)

.PHONY: reset
reset: | ~submodules
	@$(CD) keycloak && \
		$(GIT) add . && \
		$(GIT) reset --hard
	@$(MAKE) -s +patch
	@$(MAKE) -s +install

.PHONY: count
count:
	@$(CLOC) $(shell ($(GIT) ls-files && ($(GIT) lfs ls-files | $(CUT) -d' ' -f3)) | $(SORT) | $(UNIQ) -u)

.PHONY: clean
clean: ##
	-@$(MKCACHE_CLEAN)
	-@$(JEST) --clearCache $(NOFAIL)
	-@$(GIT) clean -fXd \
		$(MKPM_GIT_CLEAN_FLAGS) \
		$(YARN_GIT_CLEAN_FLAGS) \
		$(NOFAIL)

CACHE_ENVS += \
	CLOC

-include $(call actions)

endif
